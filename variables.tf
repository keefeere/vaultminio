
variable "vault_address" {
    type = string
    description = "Vault instance address"
}


variable "minio_region" {
  description = "Default MINIO region"
  default     = "us-east-1"
}

variable "minio_server" {
  description = "Default MINIO host and port"
}

variable "minio_access_key" {
  description = "MINIO user"
}

variable "minio_secret_key" {
  description = "MINIO secret user"
}

