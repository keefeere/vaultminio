
resource "minio_iam_user" "minio_user" {
  name = "${data.vault_generic_secret.minio_user.data["username"]}"
}



resource "minio_iam_policy" "policy" {
  name = "userpolicy"
  policy= <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutBucketPolicy",
        "s3:GetBucketPolicy",
        "s3:DeleteBucketPolicy",
        "s3:ListAllMyBuckets",
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::test-bucket"
      ],
      "Sid": ""
    },
    {
      "Action": [
        "s3:AbortMultipartUpload",
        "s3:DeleteObject",
        "s3:GetObject",
        "s3:ListMultipartUploadParts",
        "s3:PutObject"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::test-bucket/*"
      ],
      "Sid": ""
    }
  ]
}
EOF
}

resource "minio_iam_user_policy_attachment" "this" {
  user_name      = "${minio_iam_user.minio_user.id}"
  policy_name = "${minio_iam_policy.policy.id}"
}


resource "vault_generic_secret" "minio_user" {
   path = "kv-v2/minio"

   data_json = <<EOT
 {

   "username":   "${minio_iam_user.minio_user.id}",
   "password": "${minio_iam_user.minio_user.secret}"
 }
 EOT
}