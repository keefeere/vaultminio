resource "minio_s3_bucket" "test-bucket" {
  bucket = "test-bucket"
  acl    = "public"
}

resource "minio_s3_object" "txt_file" {
  depends_on = [minio_s3_bucket.test-bucket]
  bucket_name = minio_s3_bucket.test-bucket.bucket
  object_name = "text.txt"
  content = "TestFile"
}
