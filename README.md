### **Homework 5. Part 2. Minio provisioner**

## Minio terraform provision

Features:

*  External Vault datasource
*  Get username from Vault datasource
*  Creates minio user with generating new password
*  Outputs generated password to [Vault](https://www.vaultproject.io/) kv secret store
*  Creates bucket
*  Assign policy for user and bucket
*  Outputs created minio user data
